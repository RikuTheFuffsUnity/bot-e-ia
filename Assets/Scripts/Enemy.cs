﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Enemy : MonoBehaviour
{
    [SerializeField]
    NavMeshAgent agent;
    Transform targetToFollow;
    NavMeshPath path;

    void OnValidate()
    {
        if (!agent)
        {
            agent = GetComponent<NavMeshAgent>();
        }
    }

    void Start()
    {
        targetToFollow = GameObject.FindWithTag("Player").transform;
        path = new NavMeshPath();
    }

    void Update()
    {
        if (agent.pathPending) { return; }
        if (agent.CalculatePath(targetToFollow.position, path))
        {
            agent.SetDestination(targetToFollow.position);
            return;
        }
        //trovo un'altra destinazione temporanea
        Vector3 tempDestination = Random.insideUnitSphere * 30;
        tempDestination += transform.localPosition;

        NavMeshHit hit;
        if (NavMesh.SamplePosition(tempDestination, out hit, 1, NavMesh.AllAreas))
        {
            agent.SetDestination(hit.position);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<HealthManager>().Damage(999);
        }
    }
}
